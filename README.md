# facial-lock

OpenCV demo to lock the computer after the user looks away from it.

## Getting Started

1. Clone the repo

   ```sh
   git clone https://gitlab.com/hparzych/facial-lock.git
   ```

2. Build

   ```sh
   cmake -B build && cmake --build build
   ```

3. Run

   ```sh
   ./build/facial-lock -c config.yaml
   ```

### Prerequisites

Install a C++ compiler, cmake, Boost, OpenCV, dlib and other required libraries.

```sh
apt install build-essential cmake libopencv-dev libboost-dev liblapack-dev libsqlite3-dev libyaml-cpp-dev # Debian & Ubuntu
```

Download the face detection model from
[here](http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2).

```sh
curl http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2 | bzcat > shape_predictor_68_face_landmarks.dat
```

## License

This software is released into the public domain. See `LICENSE` for more
information.
