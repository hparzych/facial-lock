#include <deque>
#include <iostream>

#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>

#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include <boost/process.hpp>
#include <yaml-cpp/yaml.h>

using namespace std;
namespace bp = boost::process;

// Enum for the different types of face detection state
enum FaceState {
  // Uninitialized state
  FACE_UNKNOWN,
  // User is in the front of the camera
  FACE_FRONT,
  // User is facing away from the camera
  FACE_AWAY,
};

// Vector of 3D points representing a common face model
vector<cv::Point3d> face_model_3d_points{
    cv::Point3d(0.0f, 0.0f, 0.0f), // Reference point of POSIT object
    cv::Point3d(0.0f, -330.0f, -65.0f),
    cv::Point3d(-225.0f, 170.0f, -135.0f),
    cv::Point3d(225.0f, 170.0f, -135.0f),
    cv::Point3d(-150.0f, -150.0f, -125.0f),
    cv::Point3d(150.0f, -150.0f, -125.0f)};

// Get 2D points from the face object
vector<cv::Point2d> face_object_to_2d_points(dlib::full_object_detection &d) {
  return std::vector<cv::Point2d>{
      cv::Point2d(d.part(30).x(), d.part(30).y()), // Nose tip
      cv::Point2d(d.part(8).x(), d.part(8).y()),   // Chin
      cv::Point2d(d.part(36).x(), d.part(36).y()), // Left eye left corner
      cv::Point2d(d.part(45).x(), d.part(45).y()), // Right eye right corner
      cv::Point2d(d.part(48).x(), d.part(48).y()), // Left Mouth corner
      cv::Point2d(d.part(54).x(), d.part(54).y()), // Right mouth corner
  };
}

// Get the camera matrix
cv::Mat get_camera_matrix(float focal_length, cv::Point2d center) {
  return cv::Mat_<double>(3, 3) << focal_length, 0, center.x, 0, focal_length,
         center.y, 0, 0, 1;
}

// Parse command line parameters and return path to config file
string get_config_path_from_command_line(int argc, char *argv[]) {
  if (argc == 3 && string(argv[1]) == "-c")
    return string(argv[2]);
  cout << "usage: facial-lock -c <config_file>" << endl;
  exit(0);
}

// Execute a command and display it's output
void run_command(string command) {
  bp::ipstream command_out;
  try {
    bp::child c(command.c_str(), bp::std_out > command_out,
                bp::std_err > command_out);
    string line;
    while (getline(command_out, line)) {
      cerr << "[command output] " << line << endl;
    }
    c.wait();

  } catch (const bp::process_error &e) {
    cerr << "Error running command: " << e.what() << endl;
  }
}

int main(int argc, char *argv[]) {

  // Read config file
  const auto config_path = get_config_path_from_command_line(argc, argv);
  const YAML::Node config = YAML::LoadFile(config_path);

  // Load configuration parameters
  auto capture_device_num = config["capture_device_num"].as<int>();
  auto capture_device_width = config["capture_device_width"].as<int>();
  auto capture_device_height = config["capture_device_height"].as<int>();
  auto capture_device_fps = config["capture_device_fps"].as<int>();
  auto processing_image_scaling_factor =
      config["processing_image_scaling_factor"].as<float>();

  auto rotation_distance_buffer_size =
      max(1, config["rotation_distance_buffer_size"].as<int>());
  auto rotation_distance_threshold =
      config["rotation_distance_threshold"].as<double>();

  auto face_state_change_cooldown =
      config["face_state_change_cooldown"].as<int>();
  auto command_face_in_front_of_camera =
      config["command_face_in_front_of_camera"].as<string>();
  auto command_face_away_from_camera =
      config["command_face_away_from_camera"].as<string>();

  // Open capture device
  cv::VideoCapture cap;
  cap.open(config["capture_device_num"].as<int>());

  // Check if the capture device is open
  if (!cap.isOpened()) {
    cerr << "Can't open the capture device" << endl;
    return 1;
  }

  // Set capture device properties
  cap.set(cv::CAP_PROP_FRAME_WIDTH, capture_device_width);
  cap.set(cv::CAP_PROP_FRAME_HEIGHT, capture_device_height);
  cap.set(cv::CAP_PROP_FPS, capture_device_fps);

  // Load the face detector
  dlib::frontal_face_detector face_detector = dlib::get_frontal_face_detector();

  // Initialize the face model
  dlib::shape_predictor face_model;
  dlib::deserialize("./shape_predictor_68_face_landmarks.dat") >> face_model;

  // Frame buffer for displaying the captured frame on the screen
  cv::Mat frame;

  // Frame buffer for processing the captured frame
  cv::Mat frame_gray;

  // Queue for calculating the average rotation distance
  deque<double> rotation_distance_queue;

  // Current state of face detection
  FaceState current_face_state = FACE_UNKNOWN;
  int current_face_state_cooldown = face_state_change_cooldown;

  // Loop until the user exits the application
  while (true) {

    // Read frame from the capture device
    if (!cap.read(frame)) {
      cerr << "Can't read frame from capture device" << endl;
      break;
    }

    // Convert the frame to grayscale
    cv::cvtColor(frame, frame_gray, cv::COLOR_BGR2GRAY);

    // Downscale the frame to make processing faster
    if (processing_image_scaling_factor != 1.0f) {
      cv::resize(frame_gray, frame_gray, cv::Size(),
                 processing_image_scaling_factor,
                 processing_image_scaling_factor);
    }

    // Detect faces in the frame
    dlib::cv_image<unsigned char> dlib_image(frame_gray);
    vector<dlib::rectangle> faces = face_detector(dlib_image);

    // Iterate through the detected faces
    for (auto &face : faces) {

      // Get landmarks for the face region
      auto face_object = face_model(dlib_image, face);

      // Get 2D points for the face region
      vector<cv::Point2d> facepoints = face_object_to_2d_points(face_object);

      // Get the camera matrix
      cv::Mat camera_matrix = get_camera_matrix(
          frame_gray.cols,
          cv::Point2d(frame_gray.cols / 2.0, frame_gray.rows / 2.0));

      // Declare buffers for storing rotation and translation matrices
      cv::Mat rotation_vector;
      cv::Mat rotation_matrix;
      cv::Mat translation_vector;

      // Calculate the rotation matrix
      cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type);
      cv::solvePnP(face_model_3d_points, facepoints, camera_matrix, dist_coeffs,
                   rotation_vector, translation_vector);

      // Vector of 2D points of the nose end
      vector<cv::Point2d> nose_end_point2D;

      // Calculate the nose end points
      vector<cv::Point3d> nose_end_point3D{cv::Point3d(0, 0, 1000.0f)};
      cv::projectPoints(nose_end_point3D, rotation_vector, translation_vector,
                        camera_matrix, dist_coeffs, nose_end_point2D);

      // Calculate the distance between the nose and the end of the nose
      auto rotation_distance = cv::norm(facepoints[0] - nose_end_point2D[0]);

      // Draw bounding boxes around the faces
      cv::Rect rect(
          face.left() / processing_image_scaling_factor,
          face.top() / processing_image_scaling_factor,
          (face.right() - face.left()) / processing_image_scaling_factor,
          (face.bottom() - face.top()) / processing_image_scaling_factor);
      cv::rectangle(frame, rect, cv::Scalar(40, 40, 160), 2);

      // Draw a line between nose end points
      cv::line(frame, facepoints[0] / processing_image_scaling_factor,
               nose_end_point2D[0] / processing_image_scaling_factor,
               cv::Scalar(120, 240, 120), 2);

      // Draw the face landmarks on the frame
      for (int i = 0; i < face_object.num_parts(); i++) {
        cv::circle(
            frame,
            cv::Point(face_object.part(i).x() / processing_image_scaling_factor,
                      face_object.part(i).y() /
                          processing_image_scaling_factor),
            2, cv::Scalar(120, 120, 240), 1);
      }

      // Push the rotation distance to the queue
      rotation_distance_queue.push_back(rotation_distance);
      if (rotation_distance_queue.size() >= rotation_distance_buffer_size) {
        rotation_distance_queue.pop_front();
      }

      // Calculate the average rotation distance
      auto effective_rotation_distance = 0;
      for (auto &value : rotation_distance_queue) {
        effective_rotation_distance += value;
      }
      effective_rotation_distance /= rotation_distance_queue.size();

      // Handle the rotation distance
      if (rotation_distance > rotation_distance_threshold) {
        if (current_face_state == FaceState::FACE_AWAY &&
            current_face_state_cooldown < 1) {
          // The face is in front of the camera. Handle the change.
          run_command(command_face_in_front_of_camera);
          current_face_state_cooldown = face_state_change_cooldown;
        }
        current_face_state = FaceState::FACE_FRONT;
      } else {
        if (current_face_state == FaceState::FACE_FRONT &&
            current_face_state_cooldown < 1) {
          // The face is turned away from the camera. Handle the change.
          run_command(command_face_away_from_camera);
          current_face_state_cooldown = face_state_change_cooldown;
        }
        current_face_state = FaceState::FACE_AWAY;
      }

      // Display values of rotation distance
      cv::putText(
          frame, "Rotation Value: " + to_string(rotation_distance),
          cv::Point(face.left() / processing_image_scaling_factor,
                    (face.top() / processing_image_scaling_factor) - 14),
          cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(220, 220, 220));
      cv::putText(frame,
                  "Average Rotation Value: " +
                      to_string(effective_rotation_distance),
                  cv::Point(face.left() / processing_image_scaling_factor,
                            face.top() / processing_image_scaling_factor),
                  cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(220, 220, 220));
    }

    // Decrease the face state change cooldown by one
    current_face_state_cooldown = max(0, current_face_state_cooldown - 1);

    // Display the frame on the screen
    cv::imshow("facial-lock", frame);

    // Check if the user has pressed the 'q' key
    if (cv::waitKey(1) == 'q') {
      break;
    }
  }

  // Close the capture device
  cap.release();

  // Close the window
  cv::destroyAllWindows();

  return 0;
}
